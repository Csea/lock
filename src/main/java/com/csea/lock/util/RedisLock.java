package com.csea.lock.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.data.redis.core.types.Expiration;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @author Csea
 * @title AutoCloseable JDK1.7引入的新特性，相当于在finally自动执行
 * @date 2020/7/15 14:40
 */
@Slf4j
public class RedisLock implements AutoCloseable {

    private RedisTemplate redisTemplate;
    private String key;
    private String value;
    private int expireTime;

    public RedisLock(RedisTemplate redisTemplate, String key, int expireTime) {
        this.redisTemplate = redisTemplate;
        this.key = key;
        this.value = UUID.randomUUID().toString();
        this.expireTime = expireTime;
    }

    public boolean getLock() {
        RedisCallback<Boolean> redisCallback = redisConnection -> {
            // 设置NX
            RedisStringCommands.SetOption setOption = RedisStringCommands.SetOption.ifAbsent();
            // 设置过期时间
            Expiration expiration = Expiration.seconds(expireTime);
            // 序列化Key
            byte[] redisKey = redisTemplate.getKeySerializer().serialize(key);
            // 序列化Value
            byte[] redisValue = redisTemplate.getValueSerializer().serialize(value);
            // 返回 setnx操作结果
            return redisConnection.set(redisKey, redisValue, expiration, setOption);
        };

        // 获取分布式锁
        Boolean lock = (Boolean) redisTemplate.execute(redisCallback);
        return lock;
    }

    public boolean unlock() {
        // LUA脚本
        String script = "if redis.call(\"get\", KEYS[1]) == ARGV[1] then\n" +
                "  return redis.call(\"del\",KEYS[1])\n" +
                "else\n" +
                "  return 0\n" +
                "end";
        RedisScript<Boolean> redisScript = RedisScript.of(script, Boolean.class);
        List<String> keyList = Arrays.asList(key);

        Boolean result = (Boolean) redisTemplate.execute(redisScript, keyList, value);
        log.info("释放锁的结果：{}", result);
        return result;
    }

    @Override
    public void close() throws Exception {
        unlock();
    }
}
