package com.csea.lock.service.serviceImpl;

import com.csea.lock.pojo.ProductEntity;
import com.csea.lock.mapper.ProductMapper;
import com.csea.lock.service.ProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Csea
 * @since 2020-07-15
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, ProductEntity> implements ProductService {

}
