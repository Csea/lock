package com.csea.lock.service.serviceImpl;

import com.csea.lock.pojo.OrderEntity;
import com.csea.lock.mapper.OrderMapper;
import com.csea.lock.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Csea
 * @since 2020-07-15
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, OrderEntity> implements OrderService {

}
