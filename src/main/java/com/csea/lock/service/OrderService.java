package com.csea.lock.service;

import com.csea.lock.pojo.OrderEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Csea
 * @since 2020-07-15
 */
public interface OrderService extends IService<OrderEntity> {

}
