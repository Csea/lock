package com.csea.lock.service;

import com.csea.lock.util.RedisLock;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author Csea
 * @title
 * @date 2020/7/15 14:50
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class SchedulerService {

    private final RedisTemplate redisTemplate;


    @Scheduled(cron = "0/5 * * * * ?")
    public void logInfo() {

//        try (RedisLock lock = new RedisLock(redisTemplate, "Csea", 30)) {
//            if (lock.getLock()) {
////                log.info("打印了*************Csea*************");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

}
