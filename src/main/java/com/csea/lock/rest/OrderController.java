package com.csea.lock.rest;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Csea
 * @since 2020-07-15
 */
@RestController
@RequestMapping("/csea/order-entity")
public class OrderController {

}
