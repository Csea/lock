package com.csea.lock.mapper;

import com.csea.lock.pojo.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Csea
 * @since 2020-07-15
 */
public interface OrderMapper extends BaseMapper<OrderEntity> {

}
