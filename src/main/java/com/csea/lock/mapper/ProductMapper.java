package com.csea.lock.mapper;

import com.csea.lock.pojo.ProductEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Csea
 * @since 2020-07-15
 */
public interface ProductMapper extends BaseMapper<ProductEntity> {

    ProductEntity selectLock(@Param("title") String title);
}
